<?php
/**
 * Created by IntelliJ IDEA.
 * User: tapai
 * Date: 01/06/2016
 * Time: 7:06 PM
 */

require_once __DIR__ . '/../vendor/autoload.php'; // Composer autoloader
require_once __DIR__ . '/../src/zf2/Loggr.php';
require_once __DIR__ . '/../src/zf2/AbstractSql.php';
require_once __DIR__ . '/../src/Upload.php';


use helpers\src\zf2\Loggr;
use helpers\src\zf2\AbstractSql;
use helpers\src\Upload;
use Zend\Db\Adapter\Adapter;

class SqlUser extends AbstractSql
{
    protected $_table = 'user';

    public function testGetUsers($where = null, $columns = null, $orders = null) {
        $this->_select = $this->select($this->_table);
        if ($where) {
            $this->_select->where($where);
        }
        if ($columns) {
            $this->_select->columns($columns);
        }
        if ($orders) {
            $this->_select->order($orders);
        }

        $this->_sqlString = $this->buildSqlString($this->_select);
        try {
            $results = $this->adapter->query($this->_sqlString, Adapter::QUERY_MODE_EXECUTE);
            return $results->toArray();
        }
        catch (Exception $e) {
            $this->_logger->log($e->getMessage());
        }
    }
}

class Tester
{
    private $_logger;
    private $_adapter;

    public function __construct() {
        $this->_logger = new Loggr('/tmp/loggr.log');
        $this->_adapter = new \Zend\Db\Adapter\Adapter(array(
            'driver' => 'Pdo',
            'dsn' => 'mysql:dbname=abode;host=localhost',
            'username' => 'root',
            'password' => '',
            'adapters' => array(
            )
        ));
    }

    public function getColumnNames() {
        $sqlUser = new SqlUser($this->_adapter);
        return $sqlUser->getColumnNames();
    }

    public function upload($file, $encryptionKey = null) {
        if (file_exists($file)) {
            $fileData = array(
                'name' => basename($file),
                'type' => mime_content_type($file),
                'tmp_name' => $file,
                'error' => 0,
                'size' => filesize($file),
                'test' => 1
            );

            $upload = new Upload();
            $upload->setGenerateNewFileName(true);
            $upload->setMaxSize(array('width' => 640, 'height' => 640));
            $upload->setLogger($this->_logger);
            $upload->setSubDir('test');
            $upload->setEncryptionKey($encryptionKey);
            $upload->setValidMimeTypes(array('image/png', 'image/jpg', 'image/jpeg', 'image/gif', 'application/pdf'));
            $this->_logger->log("encryption key: $encryptionKey");
            return $upload->upload($fileData);
        }
    }

    public function sqlOrder() {
        $sqlUser = new SqlUser($this->_adapter);
        $sqlUser->setLogger($this->_logger);
//        $orders = json_decode('{"first_name1": "DESC"}');
//        $orders = "first_name DESC";
        $orders = array('first_name' => 'desc');
        $orders = 'first_namee';
        $users = $sqlUser->testGetUsers(null, null, $orders);
        return count($users);
    }
}

if ($argc > 1) {
    $tester = new Tester();
    $results = null;
    switch ($argv[1]) {
        case 'getColumnNames':
            $results = $tester->getColumnNames();
            break;
        case 'upload':
            if ($argc > 2) {
                $encryptionKey = $argc > 3 ? $argv[3] : null;
                $results = $tester->upload($argv[2], $encryptionKey);
            }
            else {
                echo "Missing file to upload\n";
            }
            break;
        case 'sqlOrder':
            $results = $tester->sqlOrder();
            break;
    }
    if ($results !== null) {
        var_export($results);
        echo "\n";
    }
}