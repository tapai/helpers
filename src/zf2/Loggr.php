<?php

namespace helpers\src\zf2;

use Zend\Log\Logger;
use Zend\Log\Writer\Stream;

class Loggr
{
    private $_logger;

    public function __construct($logFile = '') {
        $this->_logger = new Logger();

        if (strpos($logFile, '%date%') > -1) {
            $logFile = str_replace('%date%', strftime('%Y%m%d'), $logFile);
        }

        $writer = new Stream($logFile, null, null, 0777);
        $this->_logger->addWriter($writer);
    }

    public function log($message, $priority = Logger::INFO, $extra = array()) {
        if (is_object($message) && !method_exists($message, '__toString')) {
            $message = html_entity_decode(\Zend\Debug\Debug::dump($message, null, false));
        }
        $this->_logger->log($priority, $message, $extra);
    }
}
