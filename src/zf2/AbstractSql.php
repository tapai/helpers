<?php
/**
 * Created by IntelliJ IDEA.
 * User: tapai
 * Date: 25/04/2016
 * Time: 8:09 PM
 */

namespace helpers\src\zf2;

use Zend\Db\Adapter\Adapter;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\Metadata\Metadata;
use Zend\Db\Sql\Platform;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Where;

class AbstractSql extends Sql
{
    const LOG_LEVEL_DEBUG = 0;
    const LOG_LEVEL_PRODUCTION = 1;

    protected $_table;
    protected $_message;
    protected $_select;
    protected $_logger;

    protected $_sqlString;
    protected $_logLevel;

    public function __construct(AdapterInterface $adapter, $table = null, Platform\AbstractPlatform $sqlPlatform = null) {
        parent::__construct($adapter, $table, $sqlPlatform);

        $this->_logLevel = self::LOG_LEVEL_DEBUG;
    }

    public function setLogLevel($logLevel) {
        $this->_logLevel = $logLevel;
    }

    public function setMessage($message) {
        $this->_message = $message;
    }

    public function getMessage() {
        return $this->_message;
    }

    public function saveRecord($data) {
        $insert = $this->insert($this->_table);
        $insert->values($data);
        $this->_sqlString = $this->buildSqlString($insert);
        try {
            $this->adapter->query($this->_sqlString, Adapter::QUERY_MODE_EXECUTE);
            return $this->adapter->getDriver()->getLastGeneratedValue();
        }
        catch(\PDOException $e) {
            switch ($this->_logLevel) {
                case self::LOG_LEVEL_DEBUG:
                    $this->_message = 'AbstractSql (saveRecord) failed (' . $e->getCode() . '): ' . $e->getMessage() . ' for ' . $this->_table;
                    break;
                case self::LOG_LEVEL_PRODUCTION:
                    $this->_message = 'Failed saving record for table ' . $this->_table;
                    break;
            }
        }
        return false;
    }

    public function selectRecord() {
        return $this->select($this->_table);
    }

    public function getSelect() {
        return $this->_select;
    }

    public function getSqlString() {
        return $this->_sqlString;
    }

    public function setLogger($logger) {
        $this->_logger = $logger;
    }

    public function updateRecord($id, $data) {
        $update = $this->update($this->_table);
        $update->set($data);
        $update->where("id = $id");
        $this->_sqlString = $this->buildSqlString($update);
        try {
            $result = $this->adapter->query($this->_sqlString, Adapter::QUERY_MODE_EXECUTE);
            return true;
        }
        catch(\PDOException $e) {
            switch ($this->_logLevel) {
                case self::LOG_LEVEL_DEBUG:
                    $this->_message = 'AbstractSql (updateRecord) failed ('. $e->getCode() . '): ' . $e->getMessage();
                    break;
                case self::LOG_LEVEL_PRODUCTION:
                    $this->_message = 'Failed updating record for table ' . $this->_table;
                    break;
            }
        }
        return false;
    }

    private function _createSelect($where = null, $columns = null, $orders = null, $offset = 0, $limit = 0, $quantifier = null) {
        $select = $this->select($this->_table);
        if($where) {
            $select->where($where);
        }
        if($columns) {
            $select->columns($columns);
        }
        if($orders) {
            $select->order($orders);
        }
        if($offset > 0) {
            $select->offset($offset);
        }
        if($limit > 0) {
            $select->limit($limit);
        }
        if($quantifier) {
            $select->quantifier($quantifier);
        }
        return $select;
    }

    public function createSelect($where = null, $columns = null, $orders = null, $offset = 0, $limit = 0, $quantifier = null) {
        return $this->_createSelect($where, $columns, $orders, $offset, $limit, $quantifier);
    }

    protected function _createWhere($params = array()) {
        $where = new Where();
        if ($params) {
            foreach ($params as $key => $val) {
                if (!$this->_mapWhere($where, $key, $val)) {
                    if (is_array($val)) {
                        foreach ($val as $value) {
                            $this->_parseWhereValue($where, $key, $value);
                        }
                    }
                    else {
                        $this->_parseWhereValue($where, $key, $val);
                    }
                }
            }
        }
        return $where;
    }

    protected function _getColumnNames($table = null) {
        $metadata = new Metadata($this->getAdapter());
        return $metadata->getColumnNames($table ?: $this->_table);
    }

    private function _parseWhereValue(Where $where, $key, $val) {
        $lessPosition = strpos($val, '<');
        $greaterPosition = strpos($val, '>');
        $lessEqualPosition = strpos($val, '<=');
        $greaterEqualPosition = strpos($val, '>=');
        if ($lessEqualPosition !== false) {
            $val = substr($val, $lessEqualPosition + 2);
            $where->lessThanOrEqualTo($key, $val);
        }
        elseif ($greaterEqualPosition !== false) {
            $val = substr($val, $greaterEqualPosition + 2);
            $where->greaterThanOrEqualTo($key, $val);
        }
        elseif ($lessPosition !== false) {
            $val = substr($val, $lessPosition + 1);
            $where->lessThan($key, $val);
        }
        elseif ($greaterPosition !== false) {
            $val = substr($val, $greaterPosition + 1);
            $where->greaterThan($key, $val);
        }
        elseif ($val === null) {
            $where->isNull($key);
        }
        else {
            $where->equalTo($key, $val);
        }
    }

    protected function _mapWhere(&$where, $key, $value) {
        /** overload me */
        return false;
    }

    protected function _query($select = null) {
        if (!$select) {
            $select = $this->_select;
        }
        if ($select) {
            $this->_sqlString = $this->buildSqlString($select);
            try {
                $results = $this->adapter->query($this->_sqlString, Adapter::QUERY_MODE_EXECUTE);
                return $results->toArray();
            } catch (\Exception $e) {
                switch ($this->_logLevel) {
                    case self::LOG_LEVEL_DEBUG:
                        $this->_message = $e->getMessage();
                        break;
                    case self::LOG_LEVEL_PRODUCTION:
                        $this->_message = 'Internal Server Error';
                        break;
                    default:
                        $this->_message = 'An unknown error has occurred';
                }
                throw new \RuntimeException($this->_message, null, $e);
            }
        }
        else {
            throw new \RuntimeException('Empty select', null, null);
        }
    }

    public function setSelect($select) {
        $this->_select = $select;
    }

    public function query($select = null) {
        return $this->_query($select);
    }

    public function getRecords($where = null, $columns = null, $orders = null, $offset = 0, $limit = 0, $quantifier = null) {
        $this->_select = $this->_createSelect($where, $columns, $orders, $offset, $limit, $quantifier);
        return $this->_query();
    }

    public function getRecord($where, $columns = null) {
        $records = $this->getRecords($where, $columns);
        if($records && count($records) > 0) {
            return $records[0];
        }
        return NULL;
    }

    public function deleteRecord($data) {
        $delete = $this->delete($this->_table);
        $delete->where($data);
        $this->_sqlString = $this->buildSqlString($delete);
        $results = $this->adapter->query($this->_sqlString, Adapter::QUERY_MODE_EXECUTE);
        return true;
    }

    /**
     * works in conjunction with quantifier SQL_CALC_FOUND_ROWS
     * @return mixed
     */
    public function totalRecords() {
        $sqlString = 'SELECT FOUND_ROWS() AS total';
        $results = $this->adapter->query($sqlString, Adapter::QUERY_MODE_EXECUTE);
        if($results) {
            return intval($results->toArray()[0]['total']);
        }
    }

    public function truncate() {
        $this->_sqlString = "TRUNCATE table {$this->_table}";
        return $this->adapter->query($this->_sqlString, Adapter::QUERY_MODE_EXECUTE);
    }

    public function updateRecords($where, $data) {
        $update = $this->update($this->_table);
        $update->set($data);
        $update->where($where);
        $this->_sqlString = $this->buildSqlString($update);
        try {
            $result = $this->adapter->query($this->_sqlString, Adapter::QUERY_MODE_EXECUTE);
            return true;
        }
        catch(\PDOException $e) {
            switch ($this->_logLevel) {
                case self::LOG_LEVEL_DEBUG:
                    $this->_message = 'AbstractSql (updateRecord) failed ('. $e->getCode() . '): ' . $e->getMessage();
                    break;
                case self::LOG_LEVEL_PRODUCTION:
                    $this->_message = 'Failed updating record for table ' . $this->_table;
                    break;
            }
        }
        return false;
    }

    public function getColumnNames($table = null) {
        return $this->_getColumnNames($table);
    }
}