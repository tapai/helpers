<?php

namespace helpers\src;

class Upload
{
    private $_logger;
    private $_path;
    private $_webPath;
    private $_validMimeTypes;
    private $_error;
    private $_destination;
    private $_maxSize;
    private $_generateNewFileName;
    private $_subDir;
    private $_encryptionKey;
    private $_httpHost;

    public function __construct() {
        $this->_path = 'data/';
        $protocol = 'http';
        if (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) ||
            isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
            $protocol .= 's';
        }
        $host = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : 'localhost';
        $this->_httpHost = $protocol . '://' . $host . '/image';
        $this->_webPath = $this->_httpHost . '?filename=';
        $this->_validMimeTypes = array('image/png', 'image/jpg', 'image/jpeg', 'image/gif');
    }

    public function setLogger($logger) {
        $this->_logger = $logger;
    }

    public function getLogger() {
        return $this->_logger;
    }

    public function setPath($path) {
        $this->_path = $path;
    }

    public function getPath() {
        return $this->_path;
    }

    public function setWebPath($webPath) {
        $this->_webPath = $webPath;
    }

    public function getWebPath() {
        return $this->_webPath;
    }

    public function setValidMimeTypes($mimeTypes) {
        $this->_validMimeTypes = $mimeTypes;
    }

    public function getValidMimeTypes() {
        return $this->_validMimeTypes;
    }

    public function getError() {
        return $this->_error;
    }

    public function getDestination() {
        return $this->_destination;
    }

    public function setMaxSize($maxSize) {
        if (isset($maxSize['width']) && isset($maxSize['height'])) {
            $this->_maxSize = $maxSize;
        }
        else {
            $this->_logger->log('Max Size: [width, height]');
        }
    }

    public function setGenerateNewFileName($generateNewFileName) {
        $this->_generateNewFileName = $generateNewFileName;
    }

    public function setSubDir($subDir) {
        $this->_subDir = $subDir;
    }

    public function setEncryptionKey($encryptionKey) {
        $this->_encryptionKey = $encryptionKey;
        if ($encryptionKey) {
            $this->_webPath = $this->_httpHost . '?data=';
        }
    }

    public function setHttpHost($httpHost) {
        $this->_httpHost = $httpHost;
    }

    private function _log($what) {
        if($this->_logger) {
            $this->_logger->log($what);
        }
    }

    private function _isValidMimeType(&$mimeType, $file) {
        $valid = in_array($mimeType, $this->_validMimeTypes);
        if (!$valid && $mimeType == 'application/octet-stream') {
            $mimeType = mime_content_type($file);
            $valid = in_array($mimeType, $this->_validMimeTypes);
        }

        return $valid;
    }

    public function getImageExtensionByMimeType($mimeType) {
        $mimeTypeDetail = explode('/', $mimeType);
        $ext = '';
        if (count($mimeTypeDetail) > 1) {
            $ext = $mimeTypeDetail[1];
            if ($ext == 'vnd.ms-excel' || $ext == 'plain') {
                $ext = 'csv';
            }
        }
        return $ext;
    }

    private function _appendExt($filename, $mimeType) {
        $ext = $this->getImageExtensionByMimeType($mimeType);
        $testFilename = trim(strtolower($filename));
        if (strlen($testFilename) > strlen($ext) + 1) {
            if (substr($testFilename, strlen($testFilename) - (strlen($ext) + 1)) != ".$ext") {
                return $filename . ".$ext";
            }
        }
        return $filename;
    }

    private function _createUniqueFilename($filename, $mimeType) {
        $index = 0;
        do {
            $fileInfo = pathinfo($filename);
            $fileInfoFilename = $fileInfo['filename'];
            if ($this->_generateNewFileName) {
                $fileInfoFilename = md5(uniqid(rand(), true));
            }
            if ($fileInfo) {
                if ($index > 0) {
                    $tempFilename = $fileInfoFilename . '_' . $index;
                }
                else {
                    $tempFilename = $fileInfoFilename;
                }
                if (isset($fileInfo['extension'])) {
                    $tempFilename .= '.' . $fileInfo['extension'];
                }
            }
            else {
                if ($index > 0) {
                    $tempFilename = $fileInfoFilename . '_' . $index;
                } else {
                    $tempFilename = $fileInfoFilename;
                }
                $tempFilename = $this->_appendExt($tempFilename, $mimeType);
            }
            $destination = $this->_path . ($this->_subDir ? $this->_subDir . '/' : '') . $tempFilename;
            $index++;
        } while (file_exists($destination));
        return $tempFilename;
    }

    public function upload($fileData) {
        $filename = $fileData['name'];
        $tmpName = $fileData['tmp_name'];
        $mimeType = $fileData['type'];
        $error = intval($fileData['error']);

        if ($error === UPLOAD_ERR_OK) {
            if ($this->_isUploadedFile($tmpName, $fileData)) {
                if ($this->_isValidMimeType($mimeType, $tmpName)) {
                    $destination = $this->_path;
                    if ($this->_subDir) {
                        $destination .= $this->_subDir . DIRECTORY_SEPARATOR;
                        if (!file_exists($destination)) {
                            @mkdir($destination, 0777, true);
                        }
                    }

                    $filename = $this->_createUniqueFilename($filename, $mimeType);

                    if (file_exists($destination)) {
                        $destination .= $filename;
                        $this->_destination = $destination;
                        $result = $this->_moveUploadedFile($tmpName, $this->_destination, $fileData);
                        if ($result) {
                            if ($this->_maxSize !== null) {
                                $this->_resizeImage($this->_destination);
                            }
                            if ($this->_encryptionKey) {
                                $webPath = $this->_webPath . $this->_encryptFileLocation($this->_destination);
                            }
                            else {
                                $webPath = $this->_webPath . basename($this->_destination);
                            }
                            if ($this->_subDir) {
                                $webPath .= '&type=' . $this->_subDir;

                            }
                            if ($mimeType) {
                                if (strpos($mimeType, '/') !== false) {
                                    $mimeTypeParts = explode('/', $mimeType);
                                    $webPath .= '&file_type=' . $mimeTypeParts[1];
                                }
                                else {
                                    $webPath .= '&file_type=' . $mimeType;
                                }
                            }
                            return $webPath;
                            //                    return $this->_webPath . basename($this->_destination);
                        }
                        else {
                            $this->_error = $this->_getSystemError('move uploaded file');
                        }
                    }
                    else {
                        $this->_error = $this->_getSystemError('destination exists');
                    }
                }
                else {
                    $this->_error = 'Invalid file: ' . $mimeType;
                }
            }
            else {
                $this->_log($tmpName);
                $this->_log($fileData);
                $this->_error = $this->_getSystemError('is uploaded file');
                if (!$this->_error) {
                    $this->_error = 'No uploaded file';
                }
            }
        }
        else {
            switch ($error) {
                case UPLOAD_ERR_INI_SIZE:
                case UPLOAD_ERR_FORM_SIZE:
                    $this->_error = 'Exceeded file size limit.';
                    break;
                case UPLOAD_ERR_NO_FILE:
                    $this->_error = 'No file uploaded.';
                    break;
                case UPLOAD_ERR_PARTIAL:
                    $this->_error = 'The file was only partially uploaded.';
                    break;
                default:
                    $this->_error = $this->_getSystemError('error code: ' . $error);
                    break;
            }
        }
        $this->_log($this->_error);
        return false;
    }

    private function _encryptFileLocation($fileLocation) {
        if ($this->_encryptionKey) {
            $data = array(
                'path' => $fileLocation,
                'mime_type' => mime_content_type($fileLocation)
            );

            $jsonString = json_encode($data);
            if ($this->_encryptionKey) {
                if (class_exists('\RNCryptor\RNCryptor\Encryptor')) {
                    $encryptor = new \RNCryptor\RNCryptor\Encryptor();
                    $jsonString = $encryptor->encrypt($jsonString, $this->_encryptionKey);
                }
                else {
                    $jsonString = openssl_encrypt($jsonString, "AES-128-ECB", $this->_encryptionKey);
                }
            }

            $fileLocation = base64_encode($jsonString);
        }

        return $fileLocation;
    }

    private function _isUploadedFile($tmpName, $fileData) {
        if (isset($fileData['test'])) {
            return true;
        }
        return is_uploaded_file($tmpName);
    }

    private function _moveUploadedFile($tmpName, $destination, $fileData) {
        if (isset($fileData['test'])) {
            copy($tmpName, $destination);
            return file_exists($destination);
        }
        return @move_uploaded_file($tmpName, $destination);
    }

    private function _resizeImage($imagePath) {
        if (file_exists($imagePath)) {
            if (extension_loaded('imagick')) {
                $imagick = new \Imagick(realpath($imagePath));
                if ($imagick) {
                    $imageSize = $imagick->getImageGeometry();
                    if ($imageSize['width'] > $this->_maxSize['width'] || $imageSize['height'] > $this->_maxSize['height']) {
                        $imagick->adaptiveResizeImage($this->_maxSize['width'], $this->_maxSize['height'], true);
                        $imagick->writeImage($imagePath);
                    }
                }
                else {
                    $this->_logger->log('Imagick: Unable to load image: ' . $imagePath);
                }
            }
            else {
                $this->_logger->log('PHP extension imagick not installed');
            }
        }
    }

    private function _getSystemError($operation = null) {
        $errorMessage = 'Upload: An unknown error has occurred';
        if ($operation) {
            $errorMessage .= " ($operation)";
        }
        $error = error_get_last();
        if ($error) {
            $errorMessage = $error['message'];
        }

        return $errorMessage;
    }
}
